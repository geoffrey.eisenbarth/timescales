from collections import defaultdict

from django.shortcuts import render

from .models import JournalArticle, Book, ConferenceProceeding

# Create your views here.
def index(request):
  publication_list = (
                     list(JournalArticle.objects.all()) +
                     list(ConferenceProceeding.objects.all()) +
                     list(Book.objects.all())
                     )
  # TODO: Why is this not necessary?
  #
  #publication_list.sort(key=lambda pub : pub.pub_date, reverse=True) 

  publications_by_year = defaultdict(list)
  for pub in publication_list:
    publications_by_year[pub.pub_date.year].append(pub)
  publications_by_year = dict(publications_by_year).items()[::-1]

  context = {'publications_by_year' : publications_by_year}
  return render(request, 'publications/index.html', context)
