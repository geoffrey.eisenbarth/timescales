from django.contrib import admin

from .models import Author, JournalArticle, Book, ConferenceProceeding

#TODO: authors (ManyToOne) can't be in list display, figure out a fix

# Register your models here.

@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
  fields = ('first_name', 
            'middle_name',
            'last_name',
            'email',)
  list_display = ('first_name', 'last_name', 'email',)

@admin.register(JournalArticle)
class JournalArticleAdmin(admin.ModelAdmin):
  fields = ('authors',
            'title',
            'journal_title',
            'journal_volume',
            'journal_issue', 
            'pub_date', 
            'page_range',
            'pdf_file',)
  list_display = ('title', 'journal_title', 'pub_date',)

@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
  fields = ('authors', 
            'title', 
            'edition',
            'publisher', 
            'publisher_city', 
            'pub_date',
            'pdf_file',)
  list_display = ('title', 'publisher', 'edition', 'pub_date',)

@admin.register(ConferenceProceeding)
class ConferenceProceedingAdmin(admin.ModelAdmin):
  fields = ('authors', 
            'title', 
            'name', 
            'location', 
            'pub_date', 
            'page_range',
            'pdf_file',)
  list_display = ('title', 'name', 'pub_date',)
