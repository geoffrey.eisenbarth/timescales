from __future__ import unicode_literals
from datetime import datetime

from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _


class Author(models.Model):
  first_name = models.CharField(
    max_length=30,
  )
  middle_name = models.CharField(
    max_length=30,
    blank=True,
    null=True,
  )
  last_name = models.CharField(
    max_length=30,
  )
  email = models.EmailField(
  )

  def __str__(self):
    try: 
      context = [self.first_name[0], self.midle_name[0], self.last_name]
      initialized_name = "{}.{}. {}".format(*context)
    except:
      context = [self.first_name[0], self.last_name]
      initialized_name = "{}. {}".format(*context)
    return initialized_name

  class Meta:
    verbose_name = _("Author")
    verbose_name_plural = _("Authors")


class Publication(models.Model):
  authors = models.ManyToManyField(
    Author,
  )
  title = models.CharField(
    max_length=500,
  )
  pub_date = models.DateField(
    'date published',
    default=datetime.now,
  )
  page_range = models.CommaSeparatedIntegerField(
    max_length=16,
    default=",",
  )
  pdf_file = models.FileField(
    'file',
    upload_to='publications/',
    null=True,
    blank=True,
  )

  class Meta:
    abstract = True
    ordering = ('-pub_date',)
    verbose_name = _("Publication")
    verbose_name_plural = _("Publications")


class JournalArticle(Publication):
  journal_title = models.CharField(
    max_length=500,
  )
  journal_volume = models.IntegerField(
  )
  journal_issue = models.IntegerField(
  )

  def __str__(self):
    authors_list = ", ".join([str(author) for author in self.authors.all()])
    return '{}, "{}," {}, Vol. {} ({}), No. {}, pp. {}-{}.'.format(
                                           authors_list,
                                           self.title,
                                           self.journal_title,
                                           self.journal_volume,
                                           self.pub_date.year,
                                           self.journal_issue,
                                           self.page_range.split(",")[0],
                                           self.page_range.split(",")[1])
  class Meta:
    verbose_name = _("Journal Article")
    verbose_name_plural = _("Journal Articles")


class Book(Publication):
  publisher = models.CharField(
    max_length=500,
  )
  publisher_city = models.CharField(
    max_length=500,
  )
  edition = models.CharField(
    max_length=20,
  )

  def __str__(self):
    authors_list = ", ".join([str(author) for author in self.authors.all()])
    return '{}, {}, {}, {}, {}.'.format(authors_list,
                                        self.title,
                                        self.publisher,
                                        self.publisher_city,
                                        self.pub_date.year)
  class Meta:
    verbose_name = _("Book")
    verbose_name_plural = _("Books")


class ConferenceProceeding(Publication):
  name = models.CharField(
    max_length=500,
  )
  location = models.CharField(
    max_length=300
  )
  date = models.DateField(
    'conference date',
    default=datetime.now,
  )

  def __str__(self):
    authors_list = ", ".join([str(author) for author in self.authors.all()])
    return '{}, {}, Proceedings of the {}, {}, ({}), pp.  {}-{}'.format(
                                           authors_list,
                                           self.title,
                                           self.name,
                                           self.location,
                                           self.date.year,
                                           self.page_range.split(",")[0],
                                           self.page_range.split(",")[1])

  class Meta:
    verbose_name = _("Conference Proceeding")
    verbose_name_plural = _("Conference Proceedings")
