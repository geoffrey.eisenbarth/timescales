# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('publications', '0004_auto_20160204_0833'),
    ]

    operations = [
        migrations.RenameField(
            model_name='conferenceproceeding',
            old_name='conference_date',
            new_name='date',
        ),
        migrations.RenameField(
            model_name='conferenceproceeding',
            old_name='conference_location',
            new_name='location',
        ),
        migrations.RenameField(
            model_name='conferenceproceeding',
            old_name='conference_name',
            new_name='name',
        ),
    ]
