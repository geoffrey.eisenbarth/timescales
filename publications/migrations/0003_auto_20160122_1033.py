# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-22 16:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('publications', '0002_auto_20160122_1031'),
    ]

    operations = [
        migrations.AlterField(
            model_name='book',
            name='pdf_file',
            field=models.FileField(blank=True, null=True, upload_to='files', verbose_name='file'),
        ),
        migrations.AlterField(
            model_name='conferenceproceeding',
            name='pdf_file',
            field=models.FileField(blank=True, null=True, upload_to='files', verbose_name='file'),
        ),
        migrations.AlterField(
            model_name='journalarticle',
            name='pdf_file',
            field=models.FileField(blank=True, null=True, upload_to='files', verbose_name='file'),
        ),
    ]
