from django.contrib import admin

from tutorials.models import Tutorial, Category


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
  prepopulated_fields = {'slug' : ('title',)}

@admin.register(Tutorial)
class TutorialAdmin(admin.ModelAdmin):
  change_form_template = 'tutorials/admin/change_form.html'
  exclude = ['posted']
  prepopulated_fields = {'slug' : ('title',)}
