from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _

from tinymce import models as tinymce_models


class Category(models.Model):
  title = models.CharField(
    max_length=100,
    db_index=True,
  )
  slug = models.SlugField(
    max_length=100,
    db_index=True,
  )

  def __unicode__(self):
    return '{}'.format(self.title)

  class Meta:
    verbose_name = _("Category")
    verbose_name_plural = _("Categories")

  @models.permalink
  def get_absolute_url(self):
    return ('view_category', None, {'slug' : self.slug})

class Tutorial(models.Model):
  title = models.CharField(
    max_length=100,
    unique=True,
  )
  slug = models.SlugField(
    max_length=100,
    unique=True,
  )
  body = models.TextField(
  )
  #body = tinymce_models.HTMLField()
  posted = models.DateField(
    db_index=True,
    auto_now_add=True,
  )
  category = models.ForeignKey(
    Category,
  )

  def __unicode__(self):
    return '{}'.format(self.title)

  class Meta:
    verbose_name = _("Tutorial")
    verbose_name_plural = _("Tutorials")

  @models.permalink
  def get_absolute_url(self):
    return ('view_tutorial', None, {'slug' : self.slug})  
