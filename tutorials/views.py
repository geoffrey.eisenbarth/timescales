from django.shortcuts import render_to_response, get_object_or_404

from tutorials.models import Tutorial, Category

def index(request):
  context = {'categories'       : Category.objects.all(),
             'tutorials'        : Tutorial.objects.all(),
             'active_tutorial'  : Tutorial.objects.all()[0]}

  return render_to_response('tutorials/index.html', context)

def view_tutorial(request, slug):
  context = {'categories'       : Category.objects.all(),
             'tutorials'        : Tutorial.objects.all(),
             'active_tutorial'  : get_object_or_404(Tutorial, slug=slug)}


  return render_to_response('tutorials/index.html', context)
