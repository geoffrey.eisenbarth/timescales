import subprocess

from django.contrib import admin
from django.utils import timezone

from .models import Graphic

@admin.register(Graphic)
class GraphicAdmin(admin.ModelAdmin):
  fields = ['name', 'code',]
  list_display = ['name', 'upload_date',]

  def save_model(self, request, obj, form, change):
    obj.upload_date = timezone.now()
    slug = obj.name.replace(" ", "_")

    cmd = r'mathics -e "Export[\"files/graphics/temp_TeX.txt\", TeXForm[' + obj.code + r']]"'
    subprocess.call(cmd, shell=True)

    with open('files/graphics/temp_TeX.txt', 'r') as TeX_source:
      with open('files/graphics/' + slug + '.asy', 'w') as output:
        asy_source = TeX_source.read().replace(r"\begin{asy}","")
        asy_source = asy_source.replace(r"\end{asy}","")
        output.write(asy_source)

    cmd = r'asy -cd files/graphics/ -f png ' + slug + '.asy'
    subprocess.call(cmd, shell=True)

    obj.path = '../files/graphics/' + slug + '.png'
    obj.save()
