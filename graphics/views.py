import subprocess

from django.shortcuts import render, get_object_or_404

from graphics.models import Graphic

def index(request):
  graphics = Graphic.objects.all()
  #code = graphic.code
  
  #cmd = r'mathics -e "Export[\"files/graphics/temp_TeX.txt\", TeXForm[' + code + r']]"'
  #subprocess.call(cmd, shell=True)

  #with open('files/graphics/temp_TeX.txt', 'r') as TeX_source:
  #  with open('files/graphics/' + graphic.name + '.asy', 'w') as output:
  #    asy_source = TeX_source.read().replace(r"\begin{asy}","")
  #    asy_source = asy_source.replace(r"\end{asy}","")
  #    output.write(asy_source)


  #bash_command = r'asy -cd files/graphics/ -f png ' + graphic.name + '.asy'
  #subprocess.call(bash_command, shell=True)

  #graphic.path = '../files/graphics/' + graphic.name + '.png' 

  context = {'graphics' : graphics}
  return render(request, 'graphics/index.html', context)
