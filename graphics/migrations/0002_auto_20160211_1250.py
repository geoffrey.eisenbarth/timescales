# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('graphics', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='graphic',
            old_name='mathematica_code',
            new_name='code',
        ),
        migrations.AddField(
            model_name='graphic',
            name='name',
            field=models.CharField(default=datetime.datetime(2016, 2, 11, 18, 50, 58, 760552, tzinfo=utc), max_length=50),
            preserve_default=False,
        ),
    ]
