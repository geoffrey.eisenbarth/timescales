# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('graphics', '0002_auto_20160211_1250'),
    ]

    operations = [
        migrations.AddField(
            model_name='graphic',
            name='path',
            field=models.FilePathField(default='../files/graphics/', verbose_name=b'../files/graphics/'),
            preserve_default=False,
        ),
    ]
