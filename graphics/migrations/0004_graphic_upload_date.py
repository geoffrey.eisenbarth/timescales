# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('graphics', '0003_graphic_path'),
    ]

    operations = [
        migrations.AddField(
            model_name='graphic',
            name='upload_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 11, 19, 52, 3, 319853, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
