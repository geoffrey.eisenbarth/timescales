from django.db import models

class Graphic(models.Model):
  """
  Allows a user to create images using Mathematica code via the Mathics engine.
  """
  name = models.CharField(
    max_length=50,
  )
  code = models.TextField(
  )
  path = models.FilePathField(
    "../media/graphics/",
  )
  upload_date = models.DateTimeField(
    auto_now=True,
  )
