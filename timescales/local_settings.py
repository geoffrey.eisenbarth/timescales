# This file is exec'd from settings.py, so it has access to and can
# modify all the variables in settings.py.

# If this file is changed in development, the development server will
# have to be manually restarted because changes will not be noticed
# immediately.

DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'f_&_iq&+1g_#wc(+#@-@1fzjhzw5^p9&v_nv@!1cqdn0%9l(^@'

#DATABASES = {
#    "default": {
#        # Ends with "postgresql_psycopg2", "mysql", "sqlite3" or "oracle".
#        "ENGINE": "django.db.backends.postgresql_psycopg2",
#        # DB name or path to database file if using sqlite3.
#        "NAME": "timescales-website",
#        # Not used with sqlite3.
#        "USER": "geoffrey",
#        # Not used with sqlite3.
#        "PASSWORD": "Iwata",
#        # Set to empty string for localhost. Not used with sqlite3.
#        "HOST": "localhost",
#        # Set to empty string for default. Not used with sqlite3.
#        "PORT": "5432",
#        }
#}

###################
# DEPLOY SETTINGS #
###################

# Domains for public site
#ALLOWED_HOSTS = ["timescales.ironbeard.org,"
#                ]

# These settings are used by the default fabfile.py provided.
# Check fabfile.py for defaults.

FABRIC = {
     "DEPLOY_TOOL": "rsync",  # Deploy with "git", "hg", or "rsync"
     "SSH_USER": "geoffrey",  # VPS SSH username
     "HOSTS": ["104.131.14.96"],  # The IP address of your VPS
     "DOMAINS": ALLOWED_HOSTS,  # Edit domains in ALLOWED_HOSTS
     "REQUIREMENTS_PATH": "requirements.txt",  # Project's pip requirements
     "LOCALE": "en_US.UTF-8",  # Should end with ".UTF-8"
     "DB_PASS": "Iwata",  # Live database password
     "ADMIN_PASS": "Moscow45",  # Live admin user password
     "SECRET_KEY": SECRET_KEY,
}
