from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib.flatpages import views

urlpatterns = [
  url(r'^publications/', include('publications.urls')),
  url(r'^tutorials/', include('tutorials.urls')),
  url(r'^grappelli/', include('grappelli.urls')),
  url(r'^admin/', admin.site.urls),
  url(r'^graphics/', include('graphics.urls')),
  url(r'^(?P<url>.*/)$', views.flatpage),
]
urlpatterns += staticfiles_urlpatterns()

admin.site.site_header = 'Baylor Time Scales Group Administration'
